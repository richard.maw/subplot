use std::path::PathBuf;

use pandoc_ast::{Inline, MutVisitor};

pub struct ImageVisitor {
    images: Vec<PathBuf>,
}

impl ImageVisitor {
    pub fn new() -> Self {
        ImageVisitor { images: vec![] }
    }

    pub fn images(&self) -> Vec<PathBuf> {
        self.images.clone()
    }
}

impl MutVisitor for ImageVisitor {
    fn visit_inline(&mut self, inline: &mut Inline) {
        if let Inline::Image(_attr, _inlines, target) = inline {
            self.images.push(PathBuf::from(&target.0));
        }
    }
}
