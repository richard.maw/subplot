use crate::panhelper;
use crate::DataFile;
use crate::DataFiles;

use pandoc_ast::{Block, MutVisitor};

impl MutVisitor for DataFiles {
    fn visit_vec_block(&mut self, vec_block: &mut Vec<Block>) {
        use panhelper::is_class;
        for block in vec_block {
            match block {
                Block::CodeBlock(attr, contents) => {
                    if is_class(attr, "file") {
                        let add_newline = match panhelper::find_attr_kv(&attr, "add-newline").next()
                        {
                            None | Some("auto") => !contents.ends_with('\n'),
                            Some("yes") => true,
                            Some("no") => false,
                            _ => unreachable!(),
                        };
                        let contents = if add_newline {
                            format!("{}\n", contents)
                        } else {
                            contents.clone()
                        };
                        self.push(DataFile::new(panhelper::get_filename(attr), contents));
                    }
                }
                _ => {
                    self.visit_block(block);
                }
            }
        }
    }
}
