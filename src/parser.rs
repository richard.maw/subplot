#[deny(missing_docs)]
/// Parse a scenario snippet into logical lines.
///
/// Each logical line forms a scenario step. It may be divided into
/// multiple physical lines.
pub fn parse_scenario_snippet(snippet: &str) -> impl Iterator<Item = &str> {
    snippet.lines().filter(|line| !line.trim().is_empty())
}

#[cfg(test)]
mod test {
    use super::parse_scenario_snippet;

    fn parse_lines(snippet: &str) -> Vec<&str> {
        parse_scenario_snippet(snippet).collect()
    }

    #[test]
    fn parses_empty_snippet_into_no_lines() {
        assert_eq!(parse_lines("").len(), 0);
    }

    #[test]
    fn parses_single_line() {
        assert_eq!(parse_lines("given I am Tomjon"), vec!["given I am Tomjon"])
    }

    #[test]
    fn parses_two_lines() {
        assert_eq!(
            parse_lines("given I am Tomjon\nwhen I declare myself king"),
            vec!["given I am Tomjon", "when I declare myself king"]
        )
    }

    #[test]
    fn parses_two_lines_with_empty_line() {
        assert_eq!(
            parse_lines("given I am Tomjon\n\nwhen I declare myself king"),
            vec!["given I am Tomjon", "when I declare myself king"]
        )
    }
}
