//! Process Subplot input files.
//!
//! Capture and communicate acceptance criteria for software and systems,
//! and how they are verified, in a way that's understood by all project
//! stakeholders.

#![deny(missing_docs)]

// Handle the multiple pandoc_ast versions

#[cfg(feature = "pandoc_ast_07")]
extern crate pandoc_ast_07 as pandoc_ast;
#[cfg(feature = "pandoc_ast_08")]
extern crate pandoc_ast_08 as pandoc_ast;

mod error;
pub use error::Result;
pub use error::SubplotError;

pub mod resource;

mod graphmarkup;
pub use graphmarkup::{DotMarkup, GraphMarkup, MarkupOpts, PikchrMarkup, PlantumlMarkup};

mod datafiles;
pub use datafiles::DataFile;
pub use datafiles::DataFiles;

mod panhelper;
mod typeset;

mod visitor;
use visitor::LintingVisitor;

mod policy;
pub use policy::get_basedir_from;

mod metadata;
pub use metadata::Metadata;

mod doc;
pub use doc::Document;

mod style;
pub use style::Style;

mod scenarios;
pub use scenarios::Scenario;

mod steps;
pub use steps::{ScenarioStep, StepKind};

mod bindings;
pub use bindings::Binding;
pub use bindings::Bindings;

mod parser;
pub use parser::parse_scenario_snippet;

mod matches;
pub use matches::MatchedScenario;
pub use matches::MatchedStep;
pub use matches::MatchedSteps;
pub use matches::PartialStep;
pub use matches::StepSnippet;

mod templatespec;
pub use templatespec::TemplateSpec;

mod codegen;
pub use codegen::{generate_test_program, template_spec};
