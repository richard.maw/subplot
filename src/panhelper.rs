use pandoc_ast::Attr;

/// Is a code block marked as being of a given type?
pub fn is_class(attr: &Attr, class: &str) -> bool {
    let (_id, classes, _kvpairs) = attr;
    classes.iter().any(|s| s == class)
}

/// Utility function to find key/value pairs from an attribute
pub fn find_attr_kv<'a>(attr: &'a Attr, key: &'static str) -> impl Iterator<Item = &'a str> {
    attr.2.iter().flat_map(move |(key_, value)| {
        if key == key_ {
            Some(value.as_ref())
        } else {
            None
        }
    })
}

/// Get the filename for a fenced code block tagged .file.
///
/// The filename is the first (and presumably only) identifier for the
/// block.
pub fn get_filename(attr: &Attr) -> String {
    attr.0.to_string()
}
