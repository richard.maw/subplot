## Bug Summary

(Summarise the bug you have encountered)

## Reproduction version etc.

### Subplot version

```
(Paste the output of subplot --version)
```

### Details of environment

- **OS details**: (e.g. Debian Buster)
- **Dependency Packages**: (e.g. Pandoc 2.2.1)

<!-- Please add any extra details you can think of about your environment -->

## Inputs to reproduce

(If any particular inputs are needed to make this work, please either attach
them, describe them, or reference them here.)

## Steps to reproduce

1. Do this
2. Then this
3. Then you see the bad result

/label ~bug
